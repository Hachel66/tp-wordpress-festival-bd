<?php
/*
 * Plugin Name: Guests Plugin
 * Description: Extension d'ajout et retrait d'invités
 * Author: HAM
 * Version: 1.0.0
 */

require_once plugin_dir_path(__FILE__)."/services/TP_Database_Service.php";
require_once plugin_dir_path(__FILE__)."Guests_List.php";



class GuestPlugin
{
    public function __construct()
    {
        // créer la table invités
        register_activation_hook(__FILE__, array("TP_Database_Service", "create_db"));
        // vider la table lors de la désactivation
        register_deactivation_hook(__FILE__, array("TP_Database_Service", "empty_db"));
        // je supprime la table  lors de la suppression de l'extension
        register_uninstall_hook(__FILE__, array("TP_Database_Service", "drop_db"));

        // ajout de widget
        add_action('widgets_init', function (){
           // register_widget('nomDeClassDuWidget');
        });

        // hook de construction de menu
        add_action('admin_menu', array($this, 'add_menu_guest'));
    }

    public function add_menu_guest()
    {
        add_menu_page("Nos invités",
            "Liste des invités", "manage_options",
            "pluginGuest",
            array($this, "nos_invites"),
            "dashicons-groups",
            40 );

        add_submenu_page("pluginGuest",
            "Ajouter un invité", "Ajouter",
            "manage_options", 'addGuest',
            array($this, "nos_invites"));

    }

    public function nos_invites()
    {
        echo "<h1>". get_admin_page_title() ."</h1>";
        $db = new TP_Database_Service();

        if( $_REQUEST['page'] == 'pluginGuest' ||
            ( $_POST['firstname'] && $_POST['lastname'] && $_POST['job'] ) || ($_POST['del']) )  { // liste des invités

            // suppression via l'interface d'action directe
            if( $_REQUEST['page'] == 'pluginGuest' && $_GET['id'])
                $db->deleteGuest($_GET['id']);

            if($_POST['firstname'] && $_POST['lastname']&& $_POST['job'] )
                $db->saveGuest(); // sauvegarde avec les champs de formulaire

            echo "<form method='post'>"; // à rajouter pour que le bouton appliquer fonctionne
            $table = new Guests_List();
            $table->prepare_items();
            echo $table->display();
            echo"</form>";
        } else { // formulaire de sauvegarde
            echo "<form method='post' enctype='multipart/form-data'>".
                "<p><label for='firstname'>Prénom</label><input id='firstname' type='text' name='firstname' class='widefat' required/></p>".
                "<p><label for='lastname'>Nom</label><input id='lastname' type='text' name='lastname' class='widefat' required/></p>".
                "<p><label for='job'>Métier</label><input id='job' type='text' name='job' class='widefat' required/></p>".
                "<p><label for='facebook'>Facebook</label><input id='facebook' type='text' name='facebook' class='widefat'/></p>".
                "<p><label for='twitter'>Twitter</label><input id='twitter' type='text' name='twitter' class='widefat'/></p>".
                "<p><label for='youtube'>Youtube</label><input id='youtube' type='text' name='youtube' class='widefat'/></p>".
                "<p><label for='website'>Site web</label><input id='website' type='text' name='website' class='widefat'/></p>".
                "<p><label for='instagram'>Instagram</label><input id='instagram' type='text' name='instagram' class='widefat'/></p>".
                "<p><label for='linkedin'>LinkedIn</label><input id='linkedin' type='text' name='linkedin' class='widefat'/></p>".
                "<label for='file'>Photo :</label> " .
                "<input type='file' name='myfile' id='myfile'>" .
                "<p><input type='submit' value='Enregistrer' /></p>" . 
                "</form>";
                
            }

    }

}

new GuestPlugin();