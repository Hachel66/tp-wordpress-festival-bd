<?php


class TP_Database_Service
{
    public function __construct()
    {
    }

    public static function create_db()
    {

        global $wpdb; // objet de connexion à la BDD
        $wpdb->query(
            "CREATE TABLE IF NOT EXISTS " .
                " {$wpdb->prefix}guests ( " .
                " id INT AUTO_INCREMENT PRIMARY KEY NOT NULL, " .
                " firstname VARCHAR(150) NOT NULL, " .
                " lastname VARCHAR(200) NOT NULL, " .
                " job VARCHAR(200) NOT NULL, " . 
                " photo VARCHAR(200) NOT NULL);"    
        );

        $wpdb->query(
            "CREATE TABLE IF NOT EXISTS " .
                " {$wpdb->prefix}links ( " . 
                " id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, " .
                " guest_id INT NOT NULL, " . 
                " link VARCHAR(200) NOT NULL, " . 
                " type_id INT NOT NULL);"
        );

        $wpdb->query(
            "CREATE TABLE IF NOT EXISTS " .
                " {$wpdb->prefix}types ( " .
                " id INT AUTO_INCREMENT PRIMARY KEY NOT NULL, " .
                " name VARCHAR(50) NOT NULL, " .
                " slug VARCHAR(50) NOT NULL);"    
        );

        $countguests = $wpdb->get_var("select count(*) from {$wpdb->prefix}guests");

        if ($countguests == 0) {
            $wpdb->insert("{$wpdb->prefix}guests", [
                'firstname' => "PrénomExemple",
                'lastname' => "NomExemple",
                'job' => "jobExemple"
            ]);
        }

        $counttypes = $wpdb->get_var("select count(*) from {$wpdb->prefix}types");

        if ($counttypes == 0) {
            $wpdb->insert("{$wpdb->prefix}types", [
                'id' => "1",
                'name' => "Facebook"
            ]);
            $wpdb->insert("{$wpdb->prefix}types", [
                'id' => "2",
                'name' => "Twitter"
            ]);
            $wpdb->insert("{$wpdb->prefix}types", [
                'id' => "3",
                'name' => "Youtube"
            ]);
            $wpdb->insert("{$wpdb->prefix}types", [
                'id' => "4",
                'name' => "Site web"
            ]);
            $wpdb->insert("{$wpdb->prefix}types", [
                'id' => "5",
                'name' => "Instagram"
            ]);
            $wpdb->insert("{$wpdb->prefix}types", [
                'id' => "6",
                'name' => "LinkedIn"
            ]);
        }
    }

    /**
     * Méthode de vidage
     */
    public static function empty_db()
    {
        global $wpdb;
        $wpdb->query("TRUNCATE {$wpdb->prefix}guests;");
    }

    /**
     * Méthode de suppression
     */
    public static function drop_db()
    {
        global $wpdb;
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}guests;");
    }

    /**
     * Méthode pour retourner toute une table
     */
    public function findAll()
    {
        global $wpdb;
        $res = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}guests");
        return $res;
    }

    /**
     * Méthode de sauvegarde du formulaire
     */
    public function saveGuest()
    {
        global $wpdb;
        
        if ( ! function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );
            $uploadedfile = $_FILES['myfile'];
            $upload_overrides = array( 'test_form' => false );
            $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
            if ( $movefile ) {
                $wp_filetype = $movefile['type'];
                $filename = $movefile['file'];
                $wp_upload_dir = wp_upload_dir();
                $attachment = array(
                    'guid' => $wp_upload_dir['url'] . '/' . basename( $filename ),
                    'post_mime_type' => $wp_filetype,
                    'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
                    'post_content' => '',
                    'post_status' => 'inherit'
                );
                require_once( ABSPATH . 'wp-admin/includes/image.php' );
                $attach_id = wp_insert_attachment( $attachment, $filename);
                $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
                wp_update_attachment_metadata( $attach_id, $attach_data );
            }

        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $job = $_POST['job'];
        $photo = $attach_id;

        $facebook_link = $_POST['facebook'];
        $twitter_link = $_POST['twitter'];
        $youtube_link = $_POST['youtube'];
        $website_link = $_POST['website'];
        $instagram_link = $_POST['instagram'];
        $linkedin_link = $_POST['linkedin'];

        $row = $wpdb->get_row("select * from {$wpdb->prefix}guests where lastname = '$lastname' ;");

        if (is_null($row)) {
            $wpdb->insert("{$wpdb->prefix}guests", array(
                'firstname' => $firstname,
                'lastname' => $lastname,
                'job' => $job,
                'photo' => $photo
            ));
                
        }

        //Récupération du dernier ID

        $last_guest_id = $wpdb->insert_id;
        var_dump($last_guest_id);

        // Si champ nul, ne rien faire
        // sinon ajouter une ligne dans links avec guestID=last_insert_id, Link (post), TypeID#

        $row = $wpdb->get_row("select * from {$wpdb->prefix}links where link = '$facebook_link' ;");

        if (is_null($row)) {

            $wpdb->insert("{$wpdb->prefix}links", array(
            'guest_id' => $last_guest_id,
            'link' => $facebook_link,
            'type_id' => 1

        ));
        }

        $row = $wpdb->get_row("select * from {$wpdb->prefix}links where link = '$twitter_link' ;");

        if (is_null($row)) {
               $wpdb->insert("{$wpdb->prefix}links", array(
            'guest_id' => $last_guest_id,
            'link' => $twitter_link,
            'type_id' => 2
        )); 
        }

        $row = $wpdb->get_row("select * from {$wpdb->prefix}links where link = '$youtube_link' ;");

        if (is_null($row)) {
             $wpdb->insert("{$wpdb->prefix}links", array(
            'guest_id' => $last_guest_id,
            'link' => $youtube_link,
            'type_id' => 3
        ));
        }

        $row = $wpdb->get_row("select * from {$wpdb->prefix}links where link = '$website_link' ;");

        if (is_null($row)) {
               $wpdb->insert("{$wpdb->prefix}links", array(
            'guest_id' => $last_guest_id,
            'link' => $website_link,
            'type_id' => 4
        ));
        }

        $row = $wpdb->get_row("select * from {$wpdb->prefix}links where link = '$instagram_link' ;");

        if (is_null($row)) {
            $wpdb->insert("{$wpdb->prefix}links", array(
            'guest_id' => $last_guest_id,
            'link' => $instagram_link,
            'type_id' => 5
        ));
        }

        $row = $wpdb->get_row("select * from {$wpdb->prefix}links where link = '$linkedin_link' ;");

        if (is_null($row)) {
                $wpdb->insert("{$wpdb->prefix}links", array(
            'guest_id' => $last_guest_id,
            'link' => $linkedin_link,
            'type_id' => 6
        ));
        } 

    }

    /**
     * Methode pour retourner toute une table
     */
    // public function showGuests()
    // {
    //     global $wpdb;
    //     $res = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}guests");
    //     return $res;
    // }

    /**
     * Méthode de suppression de l'invité
     */
    public function deleteGuest($ids)
    {
        global $wpdb;

        if (!is_array($ids)) {
            $ids = array($ids);
        }

        $wpdb->query("delete from {$wpdb->prefix}guests where id in (" .
            implode(',', $ids) . ");");
    }
}
