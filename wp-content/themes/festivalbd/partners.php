<?php
$args = array(
    'post_type' => 'attachment',
    'post_status' => 'any',
    'posts_per_page' => 50,
    'post_mime_type' =>  'image/jpeg, image/png',
);
$my_query = new WP_Query($args);
?>
<section id="partners" class="section-with-bg wow fadeInUp">
    <div class="container">
        <div class="section-header">
            <h2>Nos Partenaires</h2>
        </div>
        <div class="row no-gutters partners-wrap clearfix">
            <?php
            if ($my_query->have_posts()) :
                while ($my_query->have_posts()) : $my_query->the_post();
                    $content = get_post($my_query->id);
                    if (wp_attachment_is_image($my_query->id)) :

                        $att_url = wp_get_attachment_url($my_query->id);
                        $alt_text = get_post_meta($my_query->ID, '_wp_attachment_image_alt', true);

                        if ($content->post_content === '[partenaire]') :
            ?>
                            <div class="col-lg-3 col-md-4 col-xs-6">
                                <div class="partner-logo">
                                    <img class="img-fluid" src=<?php echo $att_url; ?> width=171 ; alt=<?php echo $alt_text ?>>
                                </div>
                            </div>
            <?php
                        endif;
                    endif;
                endwhile;
            endif;
            wp_reset_postdata();
            ?>
        </div>
    </div>
</section>

<?php add_shortcode('partenaire', 'partners'); ?>