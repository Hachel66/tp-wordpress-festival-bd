<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <?php wp_head() ?>

  <title>FID&amp;BD</title>

</head>

<body>
  <header id="header" class="">
    <div class="container">

      <!-- Logo -->
      <div id="logo" class="pull-left">
        <h1><a href="http://tp-wordpress.lndo.site/#intro"><span>FID</span>&amp;<span>BD</span></a></h1>
      </div>

      <!-- menu de navigation -->
      <?php
      wp_nav_menu(array(
        'menu' => 'menu-principal',
        'theme_location' => 'header_menu',
        'menu_class' => 'nav-menu',
        'container_id' => 'nav-menu-container'
      ));
      ?>
    </div>
  </header>