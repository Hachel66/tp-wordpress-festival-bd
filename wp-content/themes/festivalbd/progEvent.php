<?php 
$date = get_post_custom_values('Date')[0];
$type = get_post_custom_values('Type')[0];
$batiment = get_post_custom_values('Batiment')[0];
$adresse = get_post_custom_values('Adresse')[0];
$horaires = get_post_custom_values('Horaires')[0];
$tarif = get_post_custom_values('Tarif')[0];

/* $date = get_post_meta(get_the_ID(), 'date', true);
$type = get_post_meta(get_the_ID(), 'type', true);
$batiment = get_post_meta(get_the_ID(), 'batiment', true);
$adresse = get_post_meta(get_the_ID(), 'adresse', true);
$horaires = get_post_meta(get_the_ID(), 'horaires', true);
$tarif = get_post_meta(get_the_ID(), 'tarif', true); */
?>



<div class="row prog-item">

    <div class="col-md-2">
        <time>
            <?php echo $date;?>
        </time>
    </div>

    <div class="col-md-10">

        <h4>
            <?php echo $type;?>
            <span><?php echo $batiment;?> (<?php echo $adresse;?>)</span>
        </h4>

        <p>
            <?php echo the_title(); ?><br>

            <?php if (!is_null( the_content() ) ) : 
                echo the_content()?><br> 
            <?php endif; ?>

            <?php if (!is_null( $horaires ) ) : 
                echo $horaires?><br> 
            <?php endif; ?>

            <?php if (!is_null( $tarif ) ) : 
                echo $tarif?><br> 
            <?php endif; ?>
        </p>
    </div>

</div>
