<!-- Footer -->
<footer id="footer">

<div class="container">
  <div class="copyright">
    ©<strong> FID&amp;BD</strong> 2020. Tous droits réservés
  </div>
  <div class="credits">
    Design et développement <a href="https://www.graphicalizer.com/" target="blank">Yacine KERROUCHE</a>
  </div>
</div>
</footer>

  <!--  Bouton retour vers le haut -->
  <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>

</body>
</html>