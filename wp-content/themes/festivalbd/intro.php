<section id="intro" style="background: url(<?php echo get_background_image() ?>) fixed top center no-repeat contain;">

    <?php
    $intro = new WP_Query(array(
        'category_name' => 'accueil'
    ));

    if ($intro->have_posts()) :

        while ($intro->have_posts()) : $intro->the_post();

        $date = get_post_custom_values('Date')[0];
        $address = get_post_custom_values('Adresse')[0];
        $video = get_post_custom_values('Video')[0];
        ?>
    
    
        <div class="intro-container wow fadeIn" style="visibility: visible;animation-name: fadeIn;">
            <h1 class="mb-4 pb-0"><?php the_title() ?></h1>
            <p class="mb-4 pb-0"><?php echo $date ?>, <?php echo $address ?></p>
    
            <a href="<?php echo $video ?>" class="venobox play-btn mb-4 vbox-item" data-vbtype="video" data-autoplay="true"></a>
            <a href="/#about" class="about-btn scrollto">À propos du Festival</a>
        </div>
    
        <?php endwhile;
    endif;?>

</section>
    