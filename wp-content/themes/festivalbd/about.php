<section id="about">

    <?php
    $about = new WP_Query(array(
        'category_name' => 'a-propos'
    ));

    if ($about->have_posts()) :

        while ($about->have_posts()) : $about->the_post();

            $where = get_post_custom_values('Où ?')[0];
            $when = get_post_custom_values('Quand ?')[0];

            ?>
            
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h2><?php the_title() ?></h2>
                        <p><?php the_content() ?></p>
                    </div>
                    <div class="col-lg-3">
                        <h3>Où ?</h3>
                        <p><?php echo $where ?></p>
                    </div>
                    <div class="col-lg-3">
                        <h3>Quand ?</h3>
                        <p><?php echo $when ?></p>
                    </div>
                </div>
            </div>

        <?php

        endwhile;
    endif;?>

</section>