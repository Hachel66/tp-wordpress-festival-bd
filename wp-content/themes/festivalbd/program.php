<section id="prog" class="section-with-bg">
    <div class="container wow fadeInUp">
        <div class="section-header">
            <h2>Le Programme</h2>
            <p>Découvrez l'intégralité de nos évènements</p>
        </div>

        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" href="#ap" role="tab" data-toggle="tab">Avant-première</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#expo" role="tab" data-toggle="tab">Expositions BD</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#cs" role="tab" data-toggle="tab">Concerts / Soirées</a>
            </li>
        </ul>

        <h3 class="sub-heading">Gratuité des expositions et des concerts / Entrée du FID : 3€</h3>

        <div class="tab-content row justify-content-center">

            <!-- Programme avant-première -->
            <div role="tabpanel" class="col-lg-9 tab-pane fade show active" id="ap">

                <?php
                $category = new WP_Query(array(
                    'category_name' => 'avant-premiere'
                ));
                while ($category->have_posts()) :  $category->the_post();
                    get_template_part('/progEvent', 'content');
                endwhile;
                ?>

            </div>

            <!-- Programme expositions -->
            <div role="tabpanel" class="col-lg-9  tab-pane fade" id="expo">

             <?php
                $category = new WP_Query(array(
                    'category_name' => 'expositions-bd'
                ));
                while ($category->have_posts()) :  $category->the_post();
                    get_template_part('/progEvent', 'content');
                endwhile;
                ?>
               
            </div>

            <!-- Programme concerts et soirées -->
            <div role="tabpanel" class="col-lg-9  tab-pane fade" id="cs">

            <?php

                $category = new WP_Query(array(
                    'category_name' => 'concert-soirees'
                ));
                while ($category->have_posts()) :  $category->the_post();
                    get_template_part('/progEvent', 'content');
                endwhile;
                ?>

            </div>

        </div>

    </div>

</section>