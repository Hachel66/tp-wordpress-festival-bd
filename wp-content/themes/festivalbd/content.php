<div class="section-header">
<h2 class="blog-post-title text-primary"><?php the_title(); ?></h2>
</div>

<p class="blog-post-meta">
</p>
<?php the_content(); ?>