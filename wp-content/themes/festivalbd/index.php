<?php get_header(); ?>

<?php wp_footer(); ?>

<?php get_header(); ?>

<!-- Section ACCUEIL -->
<?php get_template_part("intro"); ?> 

<main id="main">

  <!-- Section A PROPOS -->
  <?php get_template_part("about"); ?> 

  <!-- Section INVITES -->
  <?php get_template_part("guests"); ?>

  <!-- Section PROGRAMME -->
  <?php get_template_part("program"); ?>

  <!-- Section NOS PARTENAIRES -->
  <?php get_template_part("partners"); ?>

  <!-- Section CONTACT -->
  <?php get_template_part("contact"); ?>

</main>

<?php get_footer(); ?>