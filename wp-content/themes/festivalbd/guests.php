<section id="guests" class="wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
    <div class="container">
        <div class="section-header">
            <h2>Les Invités</h2>
            <p>Ils vous attendent pour vos dédicaces</p>
        </div>

        <div class="row">


            <?php
            global $wpdb;
            $results = $wpdb->get_results("select * from {$wpdb->prefix}guests;");
            foreach ($results as $guest) {
            ?>

                <div class="col-lg-4 col-md-6">
                    <div class="invit">
                        <img src=<?php echo wp_get_attachment_url($guest->photo) ?> alt="" class="img-fluid">
                        <div class="details">
                            <div>
                                <h3> <?php echo $guest->firstname . " " . $guest->lastname; ?> </h3>
                                <p><?php echo $guest->job; ?></p>
                            </div>
                            <div class="social">
                                <a href="https://www.facebook.com/frank.margerin.7" target="blank"><i class="fa fa-facebook"></i></a>
                                <a href="http://margerinf.free.fr/" target="blank"><i class="fa fa-globe"></i></a>
                                <a href="https://www.instagram.com/frankmargerin/" target="blank"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>

            <?php
            }
            ?>

</section>