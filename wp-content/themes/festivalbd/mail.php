<?php

$name = $_POST["name"];
$email = $_POST["email"];
$subject = $_POST["subject"];
$message = $_POST["message"];

if (!isset($name) && !isset($email) && !isset($subject) && !isset($message)) return log("Erreur de champ !");

$headers = 'From: ' . $email . ' (' . $name . ')\r\n' .
    'Reply-To: no-reply@festival-bd.com' . '\r\n' .
    'X-Mailer: PHP/' . phpversion() . 
    'Content-type: text/html; charset=utf-8' . "\r\n";

$messageDeco = 
"<html>
<head>
<link rel='preconnect' href='https://fonts.gstatic.com'>
<link href='https://fonts.googleapis.com/css2?family=Raleway:ital,wght@0,400;0,700;1,400;1,700&display=swap' rel='stylesheet'>
</head>
<body>
    <style>
    header {
        background-color: rgb(66, 108, 172);
    }
        header h1 {
        font-size: 36px;
        margin: 0;
        padding: 6px 0;
        line-height: 1;
        font-family: 'Raleway', sans-serif;
        font-weight: 700;
        letter-spacing: 3px;
        text-transform: uppercase;
      }
      
      header h1 span {
        color: #ffb302;
      }
      
      header h1 a,
      header h1 a:hover {
        color: #fff;
      }
      body {
        background-color: rgb(213, 222, 233);
    }
    </style>
    <header>
        <h1>
            <a href='http://tp-wordpress.lndo.site/#intro'><span>FID</span>&amp;<span>BD</span></a>
        </h1>
    </header>
    <main>" . $message . "</main>
</body>
</html>";

//Premier argument : boite mail du propriétaire du site
$success = mail("h.legrais@hotmail.fr", $subject, $messageDeco, $headers);

($success)  ? log("Email envoyé !") : log("Echec de l'envoi.");

header('Location: /');